//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Thu Jun  8 08:48:43 2023
//Host        : xoc2.ewi.utwente.nl running 64-bit CentOS Linux release 7.9.2009 (Core)
//Command     : generate_target kria_fir_wrapper.bd
//Design      : kria_fir_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module kria_fir_wrapper
   ();


  kria_fir kria_fir_i
       ();
endmodule
