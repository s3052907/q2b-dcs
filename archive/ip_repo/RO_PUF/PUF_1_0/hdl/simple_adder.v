`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/12/2023 03:33:54 PM
// Design Name: 
// Module Name: simple_adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module simple_adder#
	(
		// Users to add parameters here
        parameter integer ADD_BY_VALUE = 1,
		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 4
	)
	(
        input wire  clk,
        input wire [31:0] data_in,
        output wire [31:0] data_out
);

  // Modify the data (add 1 to each word)
    reg [C_S_AXI_DATA_WIDTH-1:0]	data_mod;
    
    always @( posedge clk )
    begin
        data_mod <= data_in + ADD_BY_VALUE;
    end 
    
    
    assign data_out = data_mod;


endmodule