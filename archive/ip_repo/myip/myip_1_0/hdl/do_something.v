`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/12/2023 10:06:01 AM
// Design Name: 
// Module Name: do_something
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module do_something #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 4
	)
	(
        input wire  S_AXI_ACLK,
        input wire [31:0] data_in,
        output wire [31:0] data_out
);

  // Modify the data (add 1 to each word)
    reg [C_S_AXI_DATA_WIDTH-1:0]	data_mod;
    
    always @( posedge S_AXI_ACLK )
    begin
        data_mod <= data_in + 1;
    end 
    
    
    assign data_out = data_mod;


endmodule
