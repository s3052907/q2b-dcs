-makelib xcelium_lib/xilinx_vip -sv \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib xcelium_lib/xpm -sv \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/remote/labware/packages/xilinx/vivado/2022.2/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/axi_infrastructure_v1_1_0 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_vip_v1_1_13 -sv \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/ffc2/hdl/axi_vip_v1_1_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/zynq_ultra_ps_e_vip_v1_0_13 -sv \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/abef/hdl/zynq_ultra_ps_e_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/kria_sys/ip/kria_sys_zynq_ultra_ps_e_0_0/sim/kria_sys_zynq_ultra_ps_e_0_0_vip_wrapper.v" \
  "../../../bd/kria_sys/ipshared/2fbd/hdl/PUF_v1_0_S00_AXI.v" \
  "../../../bd/kria_sys/ipshared/2fbd/hdl/simple_adder.v" \
  "../../../bd/kria_sys/ipshared/2fbd/hdl/PUF_v1_0.v" \
  "../../../bd/kria_sys/ip/kria_sys_PUF_0_0/sim/kria_sys_PUF_0_0.v" \
-endlib
-makelib xcelium_lib/lib_cdc_v1_0_2 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/proc_sys_reset_v5_0_13 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/kria_sys/ip/kria_sys_rst_ps8_0_99M_0/sim/kria_sys_rst_ps8_0_99M_0.vhd" \
-endlib
-makelib xcelium_lib/generic_baseblocks_v2_1_0 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/83df/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/83df/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/83df/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib xcelium_lib/axi_data_fifo_v2_1_26 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/3111/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_register_slice_v2_1_27 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/f0b4/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_protocol_converter_v2_1_27 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/aeb3/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_clock_converter_v2_1_26 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/b8be/hdl/axi_clock_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/blk_mem_gen_v8_4_5 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/25a8/simulation/blk_mem_gen_v8_4.v" \
-endlib
-makelib xcelium_lib/axi_dwidth_converter_v2_1_27 \
  "../../../../DCS_Project.gen/sources_1/bd/kria_sys/ipshared/4675/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/kria_sys/ip/kria_sys_auto_ds_0/sim/kria_sys_auto_ds_0.v" \
  "../../../bd/kria_sys/ip/kria_sys_auto_pc_0/sim/kria_sys_auto_pc_0.v" \
  "../../../bd/kria_sys/sim/kria_sys.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

