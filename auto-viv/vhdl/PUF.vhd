library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PUF is
	port(
		clk : in std_logic;
		reset : in std_logic;
		puf_input : in std_logic_vector(127 downto 0);
		puf_output : out std_logic_vector(127 downto 0)
	);
end PUF;

architecture arch_imp of PUF is
	constant BIT_COUNT : integer := 32;
	constant INV_COUNT : integer := 7;
	constant challenge_bitsize : integer := 16;
	constant count_clock_cycles : integer := (2**20)-1;
begin
	ro_puf_inst : entity work.puf_bit
	generic map (
		BIT_COUNT => BIT_COUNT,
		inv_count => INV_COUNT,
		challenge_bitsize => challenge_bitsize,
		count_clock_cycles => count_clock_cycles
	)
	port map(
		challenge => puf_input(challenge_bitsize-1 downto 0),
		response => puf_output(0),
		finished => puf_output(32),
		counter_1 => puf_output(64+BIT_COUNT-1 downto 64),
		counter_2 => puf_output(96+BIT_COUNT-1 downto 96),
		clk => clk,
		enable => reset,
		reset => reset
	);
end arch_imp;