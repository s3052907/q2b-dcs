set script_path [ file dirname [ file normalize [ info script ] ] ]
puts $script_path

#empty project creation
	create_project DCS_project $script_path/vivado/DCS_project -part xck26-sfvc784-2LV-c

	set_property board_part xilinx.com:kv260_som:part0:1.4 [current_project]
	set_property board_connections {som240_1_connector xilinx.com:kv260_carrier:som240_1_connector:1.3} [current_project]
	set_property target_language VHDL [current_project]

#create block diagram
	create_bd_design "kria_sys"

#add zynq ultra
	startgroup
	create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.4 zynq_ultra_ps_e_0
	endgroup

#auto connect
	apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]
#enable high-performance AXI interface
	#wrong in tutorial
	#set_property CONFIG.PSU__USE__S_AXI_GP2 {1} [get_bd_cells zynq_ultra_ps_e_0]

#create ip project
	create_peripheral caes user PUF 1.0 -dir $script_path/vivado/ip_repo
	add_peripheral_interface S00_AXI -interface_mode slave -axi_type lite [ipx::find_open_core caes:user:PUF:1.0]
	generate_peripheral -driver -bfm_example_design -debug_hw_example_design [ipx::find_open_core caes:user:PUF:1.0]
	write_peripheral [ipx::find_open_core caes:user:PUF:1.0]
	set_property  ip_repo_paths  $script_path/vivado/ip_repo/PUF_1_0 [current_project]
	update_ip_catalog -rebuild

	ipx::edit_ip_in_project -upgrade true -name edit_PUF_v1_0 -directory $script_path/vivado/ip_repo $script_path/vivado/ip_repo/PUF_1_0/component.xml

	update_compile_order -fileset sources_1

#copy to ip project
	file copy -force $script_path/PUF_v1_0_S00_AXI.vhd $script_path/vivado/ip_repo/PUF_1_0/hdl/PUF_v1_0_S00_AXI.vhd

	set vhdlfiles [glob -directory $script_path/vhdl *.vhd]
	foreach file $vhdlfiles {
		set fileName [file tail $file]
		puts "Adding file $fileName"
		set destination [file join $script_path/vivado/ip_repo/PUF_1_0/hdl $fileName]

		file copy -force $file $destination
		add_files -norecurse $destination
	}

	update_compile_order -fileset sources_1

#export package
	ipx::merge_project_changes files [ipx::current_core]
	ipx::update_checksums [ipx::current_core]
	ipx::save_core [ipx::current_core]

	ipx::update_source_project_archive -component [ipx::current_core]
	ipx::create_xgui_files [ipx::current_core]
	ipx::update_checksums [ipx::current_core]
	ipx::check_integrity [ipx::current_core]
	ipx::save_core [ipx::current_core]
	ipx::move_temp_component_back -component [ipx::current_core]
	close_project -delete
	update_ip_catalog -rebuild -repo_path $script_path/vivado/ip_repo/PUF_1_0

#add puf component
	startgroup
	create_bd_cell -type ip -vlnv caes:user:PUF:1.0 PUF_0
	endgroup

#connection automation
	apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/zynq_ultra_ps_e_0/M_AXI_HPM0_FPD} Slave {/PUF_0/S00_AXI} ddr_seg {Auto} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins PUF_0/S00_AXI]

#remove unused connection
	startgroup
	set_property CONFIG.PSU__USE__M_AXI_GP1 {0} [get_bd_cells zynq_ultra_ps_e_0]
	endgroup

#save design before write_bitstream
	save_bd_design

#create wrapper
	make_wrapper -files [get_files $script_path/vivado/DCS_project/DCS_project.srcs/sources_1/bd/kria_sys/kria_sys.bd] -top
	add_files -norecurse $script_path/vivado/DCS_project/DCS_project.gen/sources_1/bd/kria_sys/hdl/kria_sys_wrapper.vhd

#compile the files
	launch_runs impl_1 -to_step write_bitstream -jobs 24
	wait_on_runs impl_1

#closing up
close_project
exit