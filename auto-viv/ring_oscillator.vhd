library ieee;
use ieee.std_logic_1164.all;

entity ring_oscillator is
	generic(
		inv_count : integer range 3 to 9
	);
	port(
		enable : in std_logic;
		response : out std_logic
	);
end ring_oscillator;

architecture arch_imp of ring_oscillator is
	attribute DONT_TOUCH : string;
	attribute ALLOW_COMBINATORIAL_LOOPS : string;

	signal buf : std_logic_vector(inv_count-1 downto 0) := (others => '0');
	attribute DONT_TOUCH of buf : signal is "true";
	attribute ALLOW_COMBINATORIAL_LOOPS of buf : signal is "TRUE";
begin
	buf(0) <= not (enable and buf(inv_count-1));

	BUF_GEN : for i in 1 to inv_count-1 generate
		buf(i) <= not buf(i-1);
	end generate;

	response <= buf(inv_count - 1);
end arch_imp;