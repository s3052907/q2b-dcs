#!/bin/bash

set -e 
set -o pipefail

cd $(dirname "$0")

module load xilinx/vivado/2022.2
module load xilinx/vitis_hls/2022.2
module load xilinx/vitis/2022.2

bash clean.sh

vivado -mode gui -source vivado.tcl

cp -p vivado/DCS_project/DCS_project.gen/sources_1/bd/kria_sys/hw_handoff/kria_sys.hwh kria_sys.hwh
cp -p vivado/DCS_project/DCS_project.runs/impl_1/kria_sys_wrapper.bit kria_sys.bit
