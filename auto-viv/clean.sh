#!/bin/bash

set -e 
set -o pipefail

cd $(dirname "$0")

rm -rf vivado
rm -rf DCS_project

rm -f vivado.jou
rm -f vivado.log

mkdir -p vivado